import './App.css';
import UserList from './components/UserList';
import './style/style.css';

function App() {
  return (
    <div className="App">
      <UserList />
    </div>
  );
}

export default App;
